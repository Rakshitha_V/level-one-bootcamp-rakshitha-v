#include <stdio.h>
#include<conio.h>
#include <math.h>


int inputx1()
{
    float x1;
    scanf("%f",&x1);
    return x1;
}
int inputx2()
{
    float x2;
    scanf("%f",&x2);
    return x2;
}


int inputy1()
{
    float y1;
    scanf("%f",&y1);
    return y1;
}
int inputy2()
{
    float y2;
    scanf("%f",&y2);
    return y2;
}


void print (float distance)
{
    printf("Distance between two points = %f\n",distance);
}


float cal(float x1, float y1,float x2,float y2)
{
     return (sqrt(( (x2-x1)*(x2-x1) )+( (y2-y1)*(y2-y1) )));
}




int main() 
{
	float x1,y1,x2,y2;
	
	printf("Input x1: ");
	x1=inputx1();
	 
	printf("Input y1: ");
	y1=inputy1();
	
	printf("Input x2: ");
	x2=inputx2();
	
	printf("Input y2: ");
	y2=inputy2();
	
    float distance = cal(x1,y1,x2,y2);
	print(distance);
	return 0;
}